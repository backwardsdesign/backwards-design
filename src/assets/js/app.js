$(document).foundation();

// Navigation
$(document).ready(function (){

    var layout   = document.getElementById('layout'),
        menu     = document.getElementById('menu'),
        menuLink = document.getElementById('menuLink');
        
    function toggleClass(element, className) {
        var classes = element.className.split(/\s+/),
            length = classes.length,
            i = 0;

        for(; i < length; i++) {
          if (classes[i] === className) {
            classes.splice(i, 1);
            break;
          }
        }
        // The className is not found
        if (length === classes.length) {
            classes.push(className);
        }

        element.className = classes.join(' ');
    }

    menuLink.onclick = function (e) {
        var active = 'active';

        e.preventDefault();
        toggleClass(layout, active);
        toggleClass(menu, active);
        toggleClass(menuLink, active);
    };

    // If the menu is clicked, close the menu
    $('.pure-menu a').on('click',function() {
        /* Act on the event */
        var active = 'active';

        //e.preventDefault();
        toggleClass(layout, active);
        toggleClass(menu, active);
        toggleClass(menuLink, active);
        $('.activity').children('#submenu').slideUp(200);
    });
});

// SVG popups for Backward Design Steps 1 thru 3
function step1(evt){
  var popup = new Foundation.Reveal($('#bdp-step1-Modal'));  
  popup.open();
};

function step2(evt){
  var popup = new Foundation.Reveal($('#bdp-step2-Modal'));  
  popup.open();
};

function step3(evt){
  var popup = new Foundation.Reveal($('#bdp-step3-Modal'));  
  popup.open();
};

// Animations & ScrollMagic
// scrollmagic animations
var controller = new ScrollMagic.Controller();

// scale image - Anne Appointment
var scene0 = new ScrollMagic.Scene({
        triggerElement: "#anne-appointment",
        duration: 500,
        offset: -50
    })
    .setTween(".img-round1", {scale: 1, opacity: 1})
    .addTo(controller);

// scale image - Help Anne Redesign
var scene1 = new ScrollMagic.Scene({
        triggerElement: "#help-anne-redesign",
        duration: 500,
        offset: -50
    })
    .setTween(".img-round2", {scale: 1, opacity: 1})
    .addTo(controller);

// drag and drop 
jQuery(document).ready(function($) {

    // scenario-one
    var drag_items = $('.dragSquares.one .drag');
    var drop_items = $('.dnd-image-drag').find('.drop');

    // scenario-two
    var drag_items_two = $('.dragSquares.three .drag');
    var drop_items_two = $('.dnd-image-drag-two').find('.drop');
    
    // scenario-three
    var drag_items_three = $('.dnd-image-drag-three .drag');
    var drop_items_three = $('.dnd-image-drag-three').find('.drop');

    //set up drag and drop event listeners
    function setUpEventListeners() {

        // Activity One
        drag_items.each(function() {
            var thisDrag = $(this);
            thisDrag[0].addEventListener('dragstart', dragStart);
            thisDrag[0].addEventListener('drag', drag);
            thisDrag[0].addEventListener('dragend', dragEnd);
        });
        drop_items.each(function() {
            var thisDrop = $(this);

            thisDrop[0].addEventListener('dragenter', dragEnter);
            thisDrop[0].addEventListener('dragover', dragOver);
            thisDrop[0].addEventListener('dragleave', dragLeave);
            thisDrop[0].addEventListener('drop', drop);

        });

        // Activity Two
        drag_items_two.each(function() {
            var thisDrag = $(this);
            thisDrag[0].addEventListener('dragstart', dragStart);
            thisDrag[0].addEventListener('drag', drag);
            thisDrag[0].addEventListener('dragend', dragEnd);
        });
        drop_items_two.each(function() {
            var thisDrop = $(this);

            thisDrop[0].addEventListener('dragenter', dragEnter);
            thisDrop[0].addEventListener('dragover', dragOver);
            thisDrop[0].addEventListener('dragleave', dragLeave);
            thisDrop[0].addEventListener('drop', drop);

        });

        // scenario-three
        drag_items_three.each(function() {
            var thisDrag = $(this);
            thisDrag[0].addEventListener('dragstart', dragStart);
            thisDrag[0].addEventListener('drag', drag);
            thisDrag[0].addEventListener('dragend', dragEnd);
        });
        drop_items_three.each(function() {
            var thisDrop = $(this);

            thisDrop[0].addEventListener('dragenter', dragEnter);
            thisDrop[0].addEventListener('dragover', dragOver);
            thisDrop[0].addEventListener('dragleave', dragLeave);
            thisDrop[0].addEventListener('drop', drop);

        });
    }
    setUpEventListeners();

    var dragItem;

    //called as soon as the draggable starts being dragged
    //used to set up data and options
    function dragStart(event) {

        drag = event.target;
        dragItem = event.target;

        //set the effectAllowed for the drag item
        event.dataTransfer.effectAllowed = 'copy';

        var imageSrc = $(dragItem).prop('src');
        var imageHTML = $(dragItem).prop('outerHTML');

        //check for IE (it supports only 'text' or 'URL')
        try {
            event.dataTransfer.setData('text/uri-list', imageSrc);
            event.dataTransfer.setData('text/html', imageHTML);
        } catch (e) {
            event.dataTransfer.setData('text', imageSrc);
        }

        $(drag).addClass('drag-active');

    }

    //called as the draggable enters a droppable 
    //needs to return false to make droppable area valid
    function dragEnter(event) {

        var drop = this;

        //set the drop effect for this zone
        event.dataTransfer.dropEffect = 'copy';
        $(drop).addClass('drop-active');

        event.preventDefault();
        event.stopPropagation();

    }

    //called continually while the draggable is over a droppable 
    //needs to return false to make droppable area valid
    function dragOver(event) {
        
        var drop = this;

        //set the drop effect for this zone
        event.dataTransfer.dropEffect = 'copy';
        $(drop).addClass('drop-active');

        event.preventDefault();
        event.stopPropagation();
    }

    //called when the draggable was inside a droppable but then left
    function dragLeave(event) {
        var drop = this;
        $(drop).removeClass('drop-active');
    }

    //called continually as the draggable is dragged
    function drag(event) {

    }

    //called when the draggable has been released (either on droppable or not)
    //may be called on invalid or valid drop
    function dragEnd(event) {

        var drag = this;
        $(drag).removeClass('drag-active');

    }

    //called when draggable is dropped on droppable 
    //final process, used to copy data or update UI on successful drop
    function drop(event) {

        drop = this;
        $(drop).removeClass('drop-active');

        var dataList, dataHTML, dataText;

        //collect our data (based on what browser support we have)
        try {
            dataList = event.dataTransfer.getData('text/uri-list');
            dataHTML = event.dataTransfer.getData('text/html');
        } catch (e) {;
            dataText = event.dataTransfer.getData('text');
        }

        //we have access to the HTML
        if (dataHTML) {
            $(drop).empty();
            $(drop).prepend(dataHTML);
            var drag = $(drop).find('.drag');
        }
        //only have access to text (old browsers + IE)
        else {
            $(drop).empty();
            $(drop).prepend($(dragItem).clone());
            var drag = $(drop).find('.drag');
        }

        //check if this element is in the right spot
        if ( $(drop).hasClass( "one" ) || $(drop).hasClass( "two" ) ) {
            checkCorrectDrop(drop, drag);  
        }   else if ( $(drop).hasClass( "three" ) || $(drop).hasClass( "four" ) ) {
            //checkCorrectDrop(drop, drag);
            checkCorrectDropTwo(drop, drag);
            } else if ( $(drop).hasClass( "five" ) || $(drop).hasClass( "six" ) ) {
            //checkCorrectDrop(drop, drag);
            checkCorrectDropThree(drop, drag);
            }

        //see if the final image is complete
        checkCorrectFinalImage();

        event.preventDefault();
        event.stopPropagation();
    }

    /* diffieHellman.setPublicKey(public_key, [encoding]) ONE */
    //check to see if this dropped item is in the correct spot
    function checkCorrectDrop(drop, drag) {

        //check if this drop is correct
        var imageValue = $(drag).attr('data-value');
        var dropValue = $(drop).attr('data-value');
        
        // STEP 1 - first question 
        var popupCorrect1a = new Foundation.Reveal($('#activity-1a-correct-Modal'));
        var popupInCorrect1a1 = new Foundation.Reveal($('#activity-1a1-incorrect-Modal'));
        var popupInCorrect1a2 = new Foundation.Reveal($('#activity-1a2-incorrect-Modal'));
       
        // STEP 1 - second question
        var popupCorrect1b = new Foundation.Reveal($('#activity-1b-correct-Modal'));
        var popupInCorrect1b1 = new Foundation.Reveal($('#activity-1b1-incorrect-Modal'));
        var popupInCorrect1b2 = new Foundation.Reveal($('#activity-1b2-incorrect-Modal'));

        // STEP 1 //
        // If this is the first question of STEP 1 and it's correct
        if ( imageValue == 1 && dropValue == 1 ) {
            $(drop).removeClass('incorrect').addClass('correct');
            //make the dropped item no longer draggable (removing the attr)
            $(drag).attr('draggable', 'false');
            //hide the original drag item (set during dragStart), we don't need it anymore
            //$(dragItem).hide();
            // Pop up the "correct" modal for the first question of the first activity
            popupCorrect1a.open();
            // Add the text from the slide to the paragraph in the rubric
            $('div.drop.one').replaceWith('<p>' + 'Analyze how the economic, political, and legal environments affect a marketing strategy.' + '</p>');
            // Hide the draggable squares from the first slide and display the 
            // squares from the second slide.
            $('img#1').hide();
            //$('div.dragSquares.two').show();
            $('.drop.two').show();  
        
       } else if ( imageValue == 3 && dropValue == 3 ) {
            $(drop).removeClass('incorrect').addClass('correct');
            //make the dropped item no longer draggable (removing the attr)
            $(drag).attr('draggable', 'false');
            //hide the original drag item (set during dragStart), we don't need it anymore
            //$(dragItem).hide();
            // Pop up the "correct" modal for the first question of the first activity
            popupCorrect1b.open();
            // Add the text from the slide to the paragraph in the rubric
            $('div.drop.two').replaceWith('<p>' + 'Summarize how ethical considerations affect marketing decisions.' + '</p>');
            // Hide the draggable squares from the first slide and display the 
            // squares from the second slide.
            $('img#3').hide();
            //$('div.dragSquares.two').show();
            $('.drop.two').show();  
       } else if ( imageValue == 1 && dropValue == 3 ) {
            $(drop).removeClass('incorrect').addClass('correct');
            //make the dropped item no longer draggable (removing the attr)
            $(drag).attr('draggable', 'false');
            //hide the original drag item (set during dragStart), we don't need it anymore
            //$(dragItem).hide();
            // Pop up the "correct" modal for the first question of the first activity
            popupCorrect1a.open();
            // Add the text from the slide to the paragraph in the rubric
           $('div.drop.two').replaceWith('<p>' + 'Analyze how the economic, political, and legal environments affect a marketing strategy.' + '</p>');
            // Hide the draggable squares from the first slide and display the 
            // squares from the second slide.
            $('img#1').hide();
            //$('div.dragSquares.two').show();
            $('.drop.two').show();  
       } else if ( imageValue == 3 && dropValue == 1 ) {
            $(drop).removeClass('incorrect').addClass('correct');
            //make the dropped item no longer draggable (removing the attr)
            $(drag).attr('draggable', 'false');
            //hide the original drag item (set during dragStart), we don't need it anymore
            //$(dragItem).hide();
            // Pop up the "correct" modal for the first question of the first activity
            popupCorrect1b.open();
            // Add the text from the slide to the paragraph in the rubric
            $('div.drop.one').replaceWith('<p>' + 'Summarize how ethical considerations affect marketing decisions.' + '</p>');
            // Hide the draggable squares from the first slide and display the 
            // squares from the second slide.
            $('img#3').hide();
            //$('div.dragSquares.two').show();
            $('.drop.two').show();  
       } else if ( imageValue == 2 && dropValue == 1 ) {
            $(drop).removeClass('correct').addClass('incorrect');
            //make the dropped item no longer draggable (removing the attr)
            $(drag).attr('draggable', 'false');
            popupInCorrect1a1.open();
       } else if ( imageValue == 2 && dropValue == 3 ) {
            $(drop).removeClass('correct').addClass('incorrect');
            //make the dropped item no longer draggable (removing the attr)
            $(drag).attr('draggable', 'false');
            popupInCorrect1a1.open();
       } 

       if ( $('img#1').is(':hidden') && $('img#3').is(':hidden') ) {
            $( 'img#2' ).hide();
            $( '#next-one' ).show();
            $( '#drag-instructions').hide();
            $( '.workshopHeader-one').empty();
            $( '.sub-paragraph-one').html( 'You’ve helped Anne choose appropriate weekly objectives that support the overall course objective. These weekly objectives will aid in providing the necessary filter for choosing appropriate evidence and assessments in the next section.' );
       }
    }  

    /* STEP 2 */
    //check to see if this dropped item is in the correct spot
    function checkCorrectDropTwo(drop, drag) {

        //check if this drop is correct
        var imageValue = $(drag).attr('data-value');
        var dropValue = $(drop).attr('data-value');

        // Activity Two - first question 
        var popupCorrect2a = new Foundation.Reveal($('#activity-2a-correct-Modal'));
        var popupInCorrect2a1 = new Foundation.Reveal($('#activity-2a1-incorrect-Modal'));
        var popupInCorrect2a2 = new Foundation.Reveal($('#activity-2a2-incorrect-Modal'));
        // Activity Two - second question
        // var popupCorrect2b = new Foundation.Reveal($('#activity-2b-correct-Modal'));
        // var popupInCorrect2b = new Foundation.Reveal($('#activity-2b-incorrect-Modal'));

        // ACTIVITY TWO //
        // If this is the first question of the Second activity and it's correct
        if ( imageValue == dropValue ) {
            $(drop).removeClass('incorrect').addClass('correct');
            //make the dropped item no longer draggable (removing the attr)
            $(drag).attr('draggable', 'false');
            //hide the original drag item (set during dragStart), we don't need it anymore
            $(dragItem).hide();
            // Pop up the "correct" modal for the first question of the first activity
            popupCorrect2a.open();
            // Move to the next slide and change the header and paragraph underneath it
            //$('.workshopHeader-two').text('Now let’s add ONE more appropriate assignment');
            //$('.sub-paragraph-two').text('Lorem ipsum Eiusmod Excepteur aliquip irure sed culpa est quis dolore.');
            // Add the text from the slide to the paragraph in the rubric
            $('div.drop.three').replaceWith('<p><strong>' + 'Interactive Case Study:' + '</strong>' +
                                ' Write a synopsis of the legal dilemma encountered by the marketing manager in the scenario.' + '</p>');
            // Hide the draggable squares from the first slide and display the 
            // squares from the second slide.
            $( '#next-two' ).show();
            $('#drag-instructions-two').hide();
            $('div.dragSquares.three').hide();
            $('div.dragSquares.four').show();
            $('.drop.four').show();     
       }  else if ( imageValue == 5 && dropValue == 4 ) {
            $(drop).removeClass('correct').addClass('incorrect');
            popupInCorrect2a1.open();
       }  else if ( imageValue == 6 && dropValue == 4 ) {
            $(drop).removeClass('correct').addClass('incorrect');
            popupInCorrect2a2.open();
       }
    }

    /* STEP THREE */
    //check to see if this dropped item is in the correct spot
    function checkCorrectDropThree(drop, drag) {

        //check if this drop is correct
        var imageValue = $(drag).attr('data-value');
        var dropValue = $(drop).attr('data-value');

        // Activity Three - first question 
        var popupCorrect3a = new Foundation.Reveal($('#activity-3a-correct-Modal'));
        var popupInCorrect3a1 = new Foundation.Reveal($('#activity-3a1-incorrect-Modal'));
        var popupInCorrect3a2 = new Foundation.Reveal($('#activity-3a2-incorrect-Modal'));
        // Activity Three - second question
        var popupCorrect3b = new Foundation.Reveal($('#activity-3b-correct-Modal'));
        var popupInCorrect3b1 = new Foundation.Reveal($('#activity-3b1-incorrect-Modal'));
        var popupInCorrect3b2 = new Foundation.Reveal($('#activity-3b2-incorrect-Modal'));

        // ACTIVITY Three //
        // If this is the first question of the Second activity and it's correct
        if ( imageValue == dropValue ) {
            $(drop).removeClass('incorrect').addClass('correct');
            //make the dropped item no longer draggable (removing the attr)
            $(drag).attr('draggable', 'false');
            //hide the original drag item (set during dragStart), we don't need it anymore
            $(dragItem).hide();
            // Pop up the "correct" modal for the first question of the first activity
            popupCorrect3a.open();
            // Move to the next slide and change the header and paragraph underneath it
            // $('.workshopHeader-three').text('Lorem ipsum Qui in dolore.');
            $('.workshopSubHeader').text('Here\'s the start of Anne\'s course Map:').show();
            $('.anne-continues').show();
            $( '.sub-paragraph-three' ).hide();
            $( '.sub-paragraph-four' ).hide();
            $( '.sub-paragraph-five' ).hide();
            $( '.workshopHeader-three' ).hide();
            // Add the text from the slide to the paragraph in the rubric
            $('div.drop.five').replaceWith('<p><strong>' + 'Custom Multimedia:' + '</strong>' + ' Making a socially responsible marketing plan.' + '</p>' );
            // Hide the draggable squares from the first slide and display the 
            // squares from the second slide.
            $('div.dragSquares.five').hide();
            $('#drag-instructions-three').hide();
            $( '#next-three' ).show();
            //$('div.dragSquares.six').show();
            //$('.drop.six').show();     
       }  else if ( imageValue == 1 &&  dropValue == 2 ) {
            $(drop).removeClass('correct').addClass('incorrect');
            popupInCorrect3b2.open();
       }  else if ( imageValue == 3 && dropValue == 2 ) {
            $(drop).removeClass('correct').addClass('incorrect');
            popupInCorrect3a2.open();
       }
    }  
    //checks to see if the dropped images are in the correct locations
    function checkCorrectFinalImage() {

        // scenario-one
        var correctItems = drop_items.filter('.correct');
        if (correctItems.length == drop_items.length) {
            $('.message-container').empty();
            $('.message-container').append('<p>All of these statements are 100% true, but Jodi did not bring them up during her interview with Goldman Sachs because they weren\'t relevant to the position she was interviewing for. This is a great example of how the Get the Job strategy stays the same, but the details that you use change.</p>');
            $('.message-container').append('<a class="hollow secondary button" href="#scenario-two">next scenario</a>');
        } else {
            $('.message-container').empty();
        }
    }
});

// ScrollTo for moving to next drag-n-drop scenario
var s2 = $('#scenario-two'), s3 = $('#scenario-three');

$('#goToScenario2').on('click', function(){
    TweenLite.to(s2, 1, {scrollTo:{y:-50}, ease:Power2.easeOut});    
});

$('#goToScenario3').on('click', function(){
    TweenLite.to(s3, 1, {scrollTo:{y:-50}, ease:Power2.easeOut});    
});

// Remove the slide-in divs if Activity 1, 2, or 3 is clicked in the nav menu.
$(function() {
    $('.activity').click(function(){
        $('#your-own-course-one').removeClass('on');
        $('#your-own-course-two').removeClass('on');
        $('#your-own-course-three').removeClass('on');
        $('#activity-two').removeClass('on');
        $('#activity-three').removeClass('on');
        $('#what-did-you-like').removeClass('on');
        $('#your-course').removeClass('on');
    });
});


$(function(){
    $( '.nextButton.One' ).click(function(){
        $('#your-own-course-one').addClass('on');
    });
})

$(function(){
    $( '.prevButton.Two' ).click(function(){
        $('#activity-two').removeClass('on');
    });
})

$(function(){
    $( '.nextButton.Two' ).click(function(){
        $('#your-own-course-two').addClass('on');
    });
})

$(function(){
    $( '.prevButton.Three' ).click(function(){
        $('#activity-three').removeClass('on');
    });
})

$(function(){
    $( '.nextButton.Three' ).click(function(){
        $('#your-own-course-three').addClass('on');
    });
})

$(function(){
    $( '.prevActivityButton.goToStepOne' ).click(function(){
        $('#your-own-course-one').removeClass('on');
    });
})

$(function(){
    $( '.nextActivityButton.goToStepTwo' ).click(function(){
        $('#activity-two').addClass('on');
    });
})

$(function(){
    $( '.prevActivityButton.goToStepTwo' ).click(function(){
        $('#your-own-course-two').removeClass('on');
    });
})

$(function(){
    $( '.nextActivityButton.goToStepThree' ).click(function(){
        $('#activity-three').addClass('on');
    });
})

$(function(){
    $( '.prevActivityButton.goToStepThree' ).click(function(){
        $('#your-own-course-three').removeClass('on');
    });
})

$(function(){
    $( '.nextActivityButton.goToWhatDidYouLike' ).click(function(){
        $('#what-did-you-like').addClass('on');
    });
})

$(function(){
    $( '.prevActivityButton.goToOwnCourseThree' ).click(function(){
        $('#what-did-you-like').removeClass('on');
    });
})

$(function(){
    $( '.nextActivityButton.goToYourCourse' ).click(function(){
        $('#your-course').addClass('on');
    });
})

/* COURSE MAP SLIDE ANIMATION */

/* CLOSED CAPTIONING */

$('#button1').click(function(){
    $('.closed-captioning-overlay1').slideToggle();
});

$('#button2').click(function(){
    $('.closed-captioning-overlay2').slideToggle();
});

$('#button3').click(function(){
    $('.closed-captioning-overlay3').slideToggle();
});

$('#button6').click(function(){
    $('.closed-captioning-overlay6').slideToggle();
});

$('#button7').click(function(){
    $('.closed-captioning-overlay7').slideToggle();
});

$('#button10').click(function(){
    $('.closed-captioning-overlay10').slideToggle();
});

/* SLIDE ANIMATION 1 CLOSED CAPTIONING*/
$('#button11').click(function(){
    $('.closed-captioning-overlay11').slideToggle();
});

$('#button12').click(function(){
    $('.closed-captioning-overlay12').slideToggle();
});

$('#button13').click(function(){
    $('.closed-captioning-overlay13').slideToggle();
});

$('#button14').click(function(){
    $('.closed-captioning-overlay14').slideToggle();
});


// Slide 1 - Play audio once entering the Course Map Section
var scene2 = new ScrollMagic.Scene({
        triggerElement: "#slide1",
        offset: 200
    })
    .on("enter", function (event) {
        var slide1audio = document.querySelector('#slide1Audio');
        
        if ( $( "li.orbit-slide" ).first().hasClass('is-active') ) {
            console.log('I am active!');
            slide1Audio.currentTime = 0;
            slide1Audio.play();
        }   
        
        var $quote = $("#quote"),
            mySplitText = new SplitText($quote, {type:"words"}),
            splitTextTimeline = new TimelineLite();

            TweenLite.set($quote, {perspective:400});

            //kill any animations and set text back to its pre-split state
            function kill(){
                splitTextTimeline.clear().time(0);
                splitTextTimeline.delay(1);
                mySplitText.revert();
            }

            kill();

            mySplitText.split({type:"chars, words, lines"}) 
            splitTextTimeline.staggerFrom(mySplitText.chars, 0.3, {autoAlpha:0, scale:4, force3D:true}, 0.01, 0.5)
                .staggerTo(mySplitText.words, 0.3, {color:"#0c4763;", scale:0.9}, 0.1, "words")
                .staggerTo(mySplitText.words, 0.2, {color:"white", scale:1}, 0.1, "words+=0.1")
                //.staggerTo(mySplitText.lines, 0.5, {x:100, autoAlpha:0}, 0.2) 
            
            //revert the text back to its pre-split state
            $("#revert").click(function() {
                mySplitText.revert(); 
            })    
        })
    .on("leave", function(e){
        slide1Audio.pause();
    })

    .addTo(controller);

    // If the user scrolls down, pause the audio
    var scene3 = new ScrollMagic.Scene({
            triggerElement: "#help-anne-redesign",
            offset: 200
        })
        .on("enter", function (event) {
            slide1Audio.pause();
            slide3Audio.pause();
            slide7Audio.pause();
            slide9Audio.pause();
        })
        .addTo(controller);

/* ORBIT CAROUSEL FOR COURSE MAP - DETERMINE THE CURRENT SLIDE*/
function slideNumber() {
    var $slides = $('.orbit-slide');
    var $activeSlide = $slides.filter('.is-active');
    var $activeNum = $slides.index($activeSlide) + 1;

    $('.slide-number').innerHTML = $activeNum;

    var slide1Audio = document.querySelector('#slide1Audio');
    var slide2Audio = document.querySelector('#slide2Audio');
    var slide3Audio = document.querySelector('#slide3Audio');
    var slide6Audio = document.querySelector('#slide6Audio');
    var slide7Audio = document.querySelector('#slide7Audio');
    var slide8Audio = document.querySelector('#slide8Audio');
    var slide9Audio = document.querySelector('#slide9Audio');
    var slide10Audio = document.querySelector('#slide10Audio');

    if ( $activeNum == 12 ){
        $( '#second-next' ).hide();
        $( '#second-previous' ).show();
    } else if ( $activeNum == 1){
        $( '#second-previous' ).hide();
        $( '#second-next' ).show();
    }
   

    /* SLIDE 1*/
    if ( $activeNum == 1) {
        $( '#second-previous' ).hide();
        $( '#second-next' ).show();
        console.log($activeNum);
        var $quote = $("#quote"),
        mySplitText = new SplitText($quote, {type:"words"}),
        splitTextTimeline = new TimelineLite();
            
        slide1Audio.currentTime = 0;    
        slide1Audio.play();

        TweenLite.set($quote, {perspective:400});

        //kill any animations and set text back to its pre-split state
        function kill(){
          splitTextTimeline.clear().time(0);
          splitTextTimeline.delay(0.5);
          mySplitText.revert();
        }

          kill();
          mySplitText.split({type:"chars, words, lines"}) 
          splitTextTimeline.staggerFrom(mySplitText.chars, 0.3, {autoAlpha:0, scale:4, force3D:true}, 0.01, 0.5)
            .staggerTo(mySplitText.words, 0.3, {color:"#0c4763;", scale:0.9}, 0.1, "words")
            .staggerTo(mySplitText.words, 0.2, {color:"white", scale:1}, 0.1, "words+=0.1")
            //.staggerTo(mySplitText.lines, 0.5, {x:100, autoAlpha:0}, 0.2) 
        
        //revert the text back to its pre-split state
        $("#revert").click(function() {
          mySplitText.revert(); 
        })

    }   else {
            slide1Audio.pause();
        } 



    /* SLIDE 2 */
    if ( $activeNum == 2) {
        $( '#second-previous' ).show();
        $( '#second-next' ).show();
        console.log($activeNum);
        slide2Audio.currentTime = 0;   
        slide2Audio.play();

        TweenMax.to(".cloud21", 48, {right: 920, repeat: -1});
        TweenMax.to(".cloud22", 40, {right: 920, repeat: -1});
        TweenMax.to(".cloud23", 40, {right: 920, repeat: -1});
        TweenMax.to(".cloud24", 48, {right: 920, repeat: -1});
            
        // Reset the position of the clouds
        TweenMax.set(".cloud21", {right:2});
        TweenMax.set(".cloud22", {right:100});
        TweenMax.set(".cloud23", {right:220});
        TweenMax.set(".cloud24", {right:380});
        
    }   else {
            slide2Audio.pause();
        }

        var scene4 = new ScrollMagic.Scene({
            triggerElement: "#slide2"
        })
        .on("leave", function (event) {
            slide2Audio.pause();
        })
        .addTo(controller);
    
    if ( $activeNum == 3) {
         $( '#second-previous' ).show();
         $( '#second-next' ).show();
        slide3Audio.currentTime = 0;   
        slide3Audio.play();

        TweenMax.to(".cloud31", 48, {right: 920, repeat: -1});
        TweenMax.to(".cloud32", 40, {right: 920, repeat: -1});
        TweenMax.to(".cloud33", 40, {right: 920, repeat: -1});
        TweenMax.to(".cloud34", 48, {right: 920, repeat: -1});

        // Reset the position of the clouds
        TweenMax.set(".cloud31", {right:2});
        TweenMax.set(".cloud32", {right:100});
        TweenMax.set(".cloud33", {right:220});
        TweenMax.set(".cloud34", {right:380});
    }   else {
            slide3Audio.pause();
        }

    if ( $activeNum == 4) {
         $( '#second-previous' ).show();
         $( '#second-next' ).show();
        /* CLOUD ANIMATION */
        TweenMax.to(".cloud41", 50, {right: 920, repeat: -1});
        TweenMax.to(".cloud42", 50, {right: 920, repeat: -1});
        TweenMax.to(".cloud43", 40, {right: 920, repeat: -1});
        TweenMax.to(".cloud44", 40, {right: 920, repeat: -1});

        /* ANIMATE TEXT */
        TweenLite.to(".slide4Header", 1, { ease: Back.easeOut.config(1.2), y: 260 });
        TweenMax.staggerFrom(".slide4List li", .8, {delay: .8, y:-100, autoAlpha: 0, ease:Power4.easeOut}, 0.5);

        // Reset the position of the header
        TweenLite.set(".slide4Header", {clearProps:"all"});

       
        // Reset the position of the clouds
        TweenMax.set(".cloud41", {right:2});
        TweenMax.set(".cloud42", {right:100});
        TweenMax.set(".cloud43", {right:220});
        TweenMax.set(".cloud44", {right:380});
    }   

    if ( $activeNum == 5) {
         $( '#second-previous' ).show();
         $( '#second-next' ).show();
        /* CLOUD ANIMATION */
        TweenMax.to(".cloud51", 50, {right: 920, repeat: -1});
        TweenMax.to(".cloud52", 50, {right: 920, repeat: -1});
        TweenMax.to(".cloud53", 40, {right: 920, repeat: -1});
        TweenMax.to(".cloud54", 40, {right: 920, repeat: -1});
        
        /* ANIMATE TEXT */
        TweenLite.to(".slide5Header", 1, { ease: Back.easeOut.config(1.2), y: 260 });
        TweenMax.staggerFrom(".slide5List li", 0.8, {delay: .8, y:-100, autoAlpha: 0, ease:Power4.easeOut}, 0.5);

        // Reset the position of the header
        TweenLite.set(".slide5Header", {clearProps:"all"});

        TweenLite.to("h2.slide5Header", 1.7, { ease: Back.easeOut.config(1.2), y: 260 });

        // Reset the position of the clouds
        TweenMax.set(".cloud51", {right:2});
        TweenMax.set(".cloud52", {right:100});
        TweenMax.set(".cloud53", {right:220});
        TweenMax.set(".cloud54", {right:380});
    }

    if ( $activeNum == 6) {
         $( '#second-previous' ).show();
         $( '#second-next' ).show();
        slide6Audio.currentTime = 0;   
        slide6Audio.play();

        var target = $('.target');

            TweenLite.to(target, .4, {delay: .5, scale: 1.4, opacity: 1, onComplete:reverse });

            function reverse() {
                TweenLite.to(target, .4, { scale: 1 });
            }

            TweenLite.set(target, {clearProps:"all"});
    }   else {
            slide6Audio.pause();
        }

    if ( $activeNum == 7) {
         $( '#second-previous' ).show();
         $( '#second-next' ).show();
        slide7Audio.currentTime = 0;   
        slide7Audio.play();
        
        var assignment = $('.assignment');

            TweenLite.to(assignment, .4, {delay: .5, scale: 1.4, opacity: 1, onComplete:reverse });

            function reverse() {
                TweenLite.to(assignment, .4, { scale: 1 });
            }

            TweenLite.set(assignment, {clearProps:"all"});
    }   else {
            slide7Audio.pause();
        }

    if ( $activeNum == 8) {
         $( '#second-previous' ).show();
         $( '#second-next' ).show();
        /* CLOUD ANIMATION */
        TweenMax.to(".cloud81", 60, {right: 920, repeat: -1});
        TweenMax.to(".cloud82", 60, {right: 920, repeat: -1});
        TweenMax.to(".cloud83", 40, {right: 920, repeat: -1});
        TweenMax.to(".cloud84", 40, {right: 920, repeat: -1});

        /* ANIMATE TEXT */
        TweenLite.to(".slide8Header", 1, { ease: Back.easeOut.config(1.2), y: 260 });
        TweenMax.staggerFrom(".slide8List li", 1, {delay: 1, y:-100, autoAlpha: 0, ease:Power4.easeOut}, 0.5);

        // Reset the position of the header
        TweenLite.set(".slide8Header", {clearProps:"all"});

        // Reset the position of the clouds
        TweenMax.set(".cloud81", {right:2});
        TweenMax.set(".cloud82", {right:100});
        TweenMax.set(".cloud83", {right:220});
        TweenMax.set(".cloud84", {right:380});
    }   

    if ( $activeNum == 9) {
         $( '#second-previous' ).show();
         $( '#second-next' ).show();
        /* CLOUD ANIMATION */
        TweenMax.to(".cloud91", 60, {right: 920, repeat: -1});
        TweenMax.to(".cloud92", 60, {right: 920, repeat: -1});
        TweenMax.to(".cloud93", 40, {right: 920, repeat: -1});
        TweenMax.to(".cloud94", 40, {right: 920, repeat: -1});

        /* ANIMATE TEXT */
        TweenLite.to(".slide9Header", 1, { ease: Back.easeOut.config(1.2), y: 260 });
        TweenMax.staggerFrom(".slide9List li", 1, {delay: 1, y:-100, autoAlpha: 0, ease:Power4.easeOut}, 0.5);

        // Reset the position of the header
        TweenLite.set(".slide9Header", {clearProps:"all"});

        // Reset the position of the clouds
        TweenMax.set(".cloud91", {right:2});
        TweenMax.set(".cloud92", {right:100});
        TweenMax.set(".cloud93", {right:220});
        TweenMax.set(".cloud94", {right:380});
    }   

    if ( $activeNum == 10) {
         $( '#second-previous' ).show();
         $( '#second-next' ).show();
        // 10-12-2016 NEW AUDIO/NARRATION FILE TO COME
        slide10Audio.currentTime = 0;   
        slide10Audio.play();

        var book = $('.book');

            TweenLite.to(book, .4, {delay: .5, scale: 1.4, opacity: 1, onComplete:reverse });

            function reverse() {
                TweenLite.to(book, .4, { scale: 1 });
            }

            TweenLite.set(book, {clearProps:"all"});
    }   else {
            slide10Audio.pause();
        }

    if ( $activeNum == 11) {
         $( '#second-previous' ).show();
         $( '#second-next' ).show();
        // slide10Audio.currentTime = 0;   
        // slide10Audio.play();

        /* CLOUD ANIMATION */
        TweenMax.to(".cloud111", 60, {right: 920, repeat: -1});
        TweenMax.to(".cloud112", 60, {right: 920, repeat: -1});
        TweenMax.to(".cloud113", 40, {right: 920, repeat: -1});
        TweenMax.to(".cloud114", 40, {right: 920, repeat: -1});

         /* ANIMATE TEXT */
        TweenLite.to(".slide11Header", 1, { ease: Back.easeOut.config(1.2), y: 260 });
        TweenMax.staggerFrom(".slide11List li", 1, {delay: 1, y:-100, autoAlpha: 0, ease:Power4.easeOut}, 0.5);

        // Reset the position of the header
        TweenLite.set(".slide11Header", {clearProps:"all"});

        // Reset the position of the clouds
        TweenMax.set(".cloud111", {right:2});
        TweenMax.set(".cloud112", {right:100});
        TweenMax.set(".cloud113", {right:220});
        TweenMax.set(".cloud114", {right:380});
    }   else {
            //slide10Audio.pause();
        }

    if ( $activeNum == 12) {
        // slide10Audio.currentTime = 0;   
        // slide10Audio.play();

        /* CLOUD ANIMATION */
        TweenMax.to(".cloud121", 60, {right: 920, repeat: -1});
        TweenMax.to(".cloud122", 60, {right: 920, repeat: -1});
        TweenMax.to(".cloud123", 40, {right: 920, repeat: -1});
        TweenMax.to(".cloud124", 40, {right: 920, repeat: -1});

         /* ANIMATE TEXT */
        TweenLite.to(".slide12Header", 1, { ease: Back.easeOut.config(1.2), y: 260 });
        TweenMax.staggerFrom(".slide12List li", 1, {delay: 1, y:-100, autoAlpha: 0, ease:Power4.easeOut}, 0.5);

        // Reset the position of the header
        TweenLite.set(".slide11Header", {clearProps:"all"});

        // Reset the position of the clouds
        TweenMax.set(".cloud121", {right:2});
        TweenMax.set(".cloud122", {right:100});
        TweenMax.set(".cloud123", {right:220});
        TweenMax.set(".cloud124", {right:380});
    }   else {
            //slide10Audio.pause();
        }
}


$('[data-orbit]').on('slidechange.zf.orbit', slideNumber);

/* SLIDE ANIMATION 1 */
var slider = $('.bxslider').bxSlider();

var animation1slide1Audio = document.querySelector('#animation1-slide1Audio');
var animation1slide2Audio = document.querySelector('#animation1-slide2Audio');
var animation1slide3Audio = document.querySelector('#animation1-slide3Audio');
var animation1slide4Audio = document.querySelector('#animation1-slide4Audio');
var current = slider.getCurrentSlide() + 1;

if ( current == 1 ) {
    $('#first-previous').hide();
    animation1slide1Audio.currentTime = 0;
    animation1slide1Audio.play();       
    //console.log(current);
} else if ( current != 1 ) {
    $('#first-previous').show();
} 

/* CHECK CURRENT SLIDE WHEN USER CLICKS ON PREVIOUS OR NEXT */
$('#first-previous').click(function(){
    var current = slider.getCurrentSlide() + 1;
    
    if ( current == 1 ) {
        $('#first-previous').hide();
        animation1slide1Audio.currentTime = 0;
        animation1slide1Audio.play();
        animation1slide2Audio.pause();
        animation1slide3Audio.pause();
        animation1slide4Audio.pause();
        
    } else if ( current == 2 ) {
        $('#first-previous').show();
        animation1slide2Audio.currentTime = 0;
        animation1slide2Audio.play();
        animation1slide1Audio.pause();
        animation1slide3Audio.pause();
        animation1slide4Audio.pause();
        
    } else if ( current == 3) {
        animation1slide3Audio.currentTime = 0;
        animation1slide3Audio.play();
        animation1slide1Audio.pause();
        animation1slide2Audio.pause();
        animation1slide4Audio.pause();
      
    } else if ( current == 4) {
        $('#first-next').show();
        animation1slide4Audio.currentTime = 0;
        animation1slide4Audio.play();
        animation1slide1Audio.pause();
        animation1slide2Audio.pause();
        animation1slide3Audio.pause();
    }; 
});

$('#first-next').click(function(){
    var current = slider.getCurrentSlide() + 1;
    console.log(current);
    if ( current == 1 ) {
        $('#first-next').show();
        animation1slide1Audio.currentTime = 0;
        animation1slide1Audio.play();       
        animation1slide2Audio.pause();
        animation1slide3Audio.pause();
        animation1slide4Audio.pause();
        console.log('Your current was one');
    } else if ( current == 2) {
        $('#first-previous').show();
        $('#first-next').show();
        animation1slide2Audio.currentTime = 0;
        animation1slide2Audio.play();       
        animation1slide1Audio.pause();
        animation1slide3Audio.pause();
        animation1slide4Audio.pause();
        console.log('Your current was two');
    } else if ( current == 3) {
        $('#first-previous').show();
        $('#first-next').show();
        animation1slide3Audio.currentTime = 0;
        animation1slide3Audio.play();
        animation1slide1Audio.pause();
        animation1slide2Audio.pause();
        animation1slide4Audio.pause();
        console.log('Your current was three');
    } else if ( current == 4) {
        $('#first-previous').show();
        $('#first-next').show();
        animation1slide4Audio.currentTime = 0;
        animation1slide4Audio.play();
        animation1slide1Audio.pause();
        animation1slide2Audio.pause();
        animation1slide3Audio.pause();
        console.log('Your current was four');
    } else if ( current == 5 ){
        $('#first-previous').show();
        $('#first-next').hide();
        animation1slide1Audio.pause();
        animation1slide2Audio.pause();
        animation1slide3Audio.pause();
        animation1slide4Audio.pause();
    };

});

/* CHECK CURRENT SLIDE WHEN USER CLIKS ON ROUND BUTTONS*/
$( '.bx-pager-link' ).click(function() {
    var current = $('.bx-pager-link').index( this ) + 1;
    console.log( 'Current: ' + current );

    if ( current == 1 ) {
        $('#first-previous').hide();
        $('#first-next').show();
        animation1slide1Audio.currentTime = 0;
        animation1slide1Audio.play();
        animation1slide2Audio.pause();
        animation1slide3Audio.pause();
        animation1slide4Audio.pause();
        console.log('Your current was one');
    } else if ( current == 2) {
        $('#first-previous').show();
        $('#first-next').show();
        animation1slide2Audio.currentTime = 0;
        animation1slide2Audio.play();
        animation1slide1Audio.pause();
        animation1slide3Audio.pause();
        animation1slide4Audio.pause();
        console.log('Your current was two');
    } else if ( current == 3) {
        $('#first-previous').show();
        $('#first-next').show();
        animation1slide3Audio.currentTime = 0;
        animation1slide3Audio.play();
        animation1slide1Audio.pause();
        animation1slide2Audio.pause();
        animation1slide4Audio.pause();
        console.log('Your current was three');
    } else if ( current == 4) {
        $('#first-previous').show();
        $('#first-next').show();
        animation1slide4Audio.currentTime = 0;
        animation1slide4Audio.play();
        animation1slide1Audio.pause();
        animation1slide2Audio.pause();
        animation1slide3Audio.pause();
        console.log('Your current was four');
    } else if ( current == 5 ) {
        $('#first-previous').show();
        $('#first-next').hide();
        animation1slide1Audio.pause();
        animation1slide2Audio.pause();
        animation1slide3Audio.pause();
        animation1slide4Audio.pause();

    };
});


var controller = new ScrollMagic.Controller();

// If the user scrolls down, pause the audio.
var scene4 = new ScrollMagic.Scene({
        triggerElement: "#tale-two-courses",
        offset: 200
    })
    .on('start', function () {
        animation1slide1Audio.pause();
        animation1slide2Audio.pause();
        animation1slide3Audio.pause();
        animation1slide4Audio.pause();
    })    
    .addTo(controller);

/* MODALS FOR YOUR OWN COURSE 1*/
var notSureOne = new Foundation.Reveal($('#not-sure-1-Modal'));
var somewhatOne = new Foundation.Reveal($('#somewhat-1-Modal'));
var veryOne = new Foundation.Reveal($('#very-1-Modal'));

$( "label[for='checkbox1']" ).click(function() {
    /* Act on the event */
    notSureOne.open();
});

$( "label[for='checkbox5']" ).click(function() {
    /* Act on the event */
    somewhatOne.open();
});

$( "label[for='checkbox6']" ).click(function() {
    /* Act on the event */
    veryOne.open();
});